/**
 * View and Selection of Gnomos
 * */

import React from 'react';
import {
    Text,
    View,
    Picker,
    Image,
    Alert,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import styles from '../styles';

export default class ViewHome extends React.Component {

    /**
     * hidden Superior bar
     * */
    static navigationOptions = {
        title: 'Gnomos',
        header: null
    };

    constructor (props) {
        super(props);

        this.state={
            ...this.props,
            data: null,
            filterName: null,
            filterWork: null,
            filterFriend: null,
        }
    }

    async componentDidMount(){
        await this.getData();
    }



    /**
     * get Data json from url
     * */
    async getData(){

        var url = 'https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json';

        let response = await fetch(url, {
                method: 'get',
                headers: {
                    'Content-Type': 'application/json'
                },
            })

        let data = await response.json();

        if(data.Brastlewark){
            this.setState({
                data: data.Brastlewark,
            },()=>{ })
            return true;

        }else{
            Alert.alert(
                "Attention",
                "The format isn't correct"
            );
            return false;
        }

    }

    viewGnomo(gnomo){
        this.props.navigation.navigate('ProfileGnomo',{
            gnomo:gnomo
        });
    }


    /**
     * change select options by name
     * */
    changeOptionName(index, value) {
        if(value=='All'){
            value = null;
        }
        this.setState({
            filterName: value,
        })
    }

    /**
     * change select options by work
     * */
    changeOptionWork(index, value) {
        if(value=='All'){
            value = null;
        }
        this.setState({
            filterWork: value,
        })
    }

    /**
     * change select options by friend
     * */
    changeOptionFriend(index, value) {
        if(value=='All'){
            value = null;
        }
        this.setState({
            filterFriend: value,
        })
    }

    render() {
        var {
            data,
            filterName,
            filterWork,
            filterFriend
        } = this.state;

        var selectNames = ['All'];
        var selectFriends = ['All'];
        var selectWorks = ['All'];
        var gnomos = []

        if(data){

            /**
             * sort List for Name
             */
            data.sort(function(a,b){
                return (a.name).localeCompare(b.name);
            });


            /**
             * create new list for view
             */
            data.map((gnomo)=> {
                var name = gnomo.name.trim();
                var friends = gnomo.friends;
                var works = gnomo.professions;
                var addGnomo = false;

                var addGnomoName = true;
                if(filterName){
                    addGnomoName = false;
                }

                var addGnomoWork = true;
                if(filterWork){
                    addGnomoWork = false;
                }

                var addGnomoFriend = true;
                if(filterFriend){
                    addGnomoFriend = false;
                }

                if (selectNames.indexOf(name) == -1) {
                    selectNames.push(name);
                }

                //Filters
                if(name==filterName){
                    addGnomoName = true;
                }

                /***
                 *set Picker Work and filter for Work
                 */
                for(var work in works){
                    var workName = works[work].trim();

                    if (selectWorks.indexOf(workName) == -1) {
                        selectWorks.push(workName);
                    }

                    if(workName==filterWork){
                        addGnomoWork = true;
                    }
                }

                /***
                 *set Picker Friend and filter for friend
                 */
                for(var friend in friends){
                    var friendName = friends[friend].trim();

                    if (selectFriends.indexOf(friendName) == -1) {
                        selectFriends.push(friendName);
                    }

                    if(friendName==filterFriend){
                        addGnomoFriend = true;
                    }
                }

                if(addGnomoName && addGnomoWork && addGnomoFriend){
                    addGnomo = true;
                }

                if(addGnomo){
                    gnomos.push(
                        <TouchableOpacity
                            onPress={()=>{this.viewGnomo(gnomo)}}>
                            <View style={styles.item}>
                                <Text style={styles.text}>{name}</Text>
                            </View>
                        </TouchableOpacity>
                    );
                }
            })
        }



        /**
         * sort Picker for Name
         */
        selectNames.sort(function(a,b){
            return (a).localeCompare(b);
        });

        /**
         * sort Picker for Friends
         */
        selectFriends.sort(function(a,b){
            return (a).localeCompare(b);
        });

        /**
         * sort Picker for Works
         */
        selectWorks.sort(function(a,b){
            return (a).localeCompare(b);
        });

        return (
            <View style={styles.window}>
                <View style={{height:70}}>
                    <View style={styles.pickers}>

                        {/*
                        <View style={styles.itemPicker}>
                            <Text style={[styles.text,styles.title]}>
                                By Name
                            </Text>
                            <Picker
                                selectedValue={filterName}
                                style={[styles.text,styles.picker]}
                                textStyle={[styles.text, {fontSize:12}]}
                                onValueChange={(itemValue, itemIndex) => { this.changeOptionName(itemIndex, itemValue) }}>
                                {selectNames.map((option, index)=>{
                                    return (
                                        <Picker.Item
                                            label={option}
                                            value={option} />
                                    )
                                })}
                            </Picker>
                        </View>
                        */}

                        <View style={styles.itemPicker}>
                            <Text style={[styles.text,styles.title]}>
                                By Friend
                            </Text>
                            <Picker
                                selectedValue={filterFriend}
                                style={[styles.text,styles.picker]}
                                textStyle={[styles.text, {fontSize:12}]}
                                onValueChange={(itemValue, itemIndex) => { this.changeOptionFriend(itemIndex, itemValue) }}>
                                {selectFriends.map((option, index)=>{
                                    return (
                                        <Picker.Item
                                            label={option}
                                            value={option} />
                                    )
                                })}
                            </Picker>
                        </View>
                        <View style={styles.itemPicker}>
                            <Text style={[styles.text,styles.title]}>
                                By Work
                            </Text>
                            <Picker
                                selectedValue={filterWork}
                                style={[styles.text,styles.picker]}
                                textStyle={[styles.text, {fontSize:12}]}
                                onValueChange={(itemValue, itemIndex) => { this.changeOptionWork(itemIndex, itemValue) }}>
                                {selectWorks.map((option, index)=>{
                                    return (
                                        <Picker.Item
                                            label={option}
                                            value={option} />
                                    )
                                })}
                            </Picker>
                        </View>
                    </View>
                </View>

                <View>
                    {(!data) &&
                        <Text style={[styles.text,styles.title, {alignSelf:'center'}]}>
                            Waiting Data
                        </Text>
                    }

                    {(gnomos.length>0) &&
                        <Text style={[styles.text, styles.title, {alignSelf:'center'}]}>
                            {gnomos.length} Gnome found
                        </Text>
                    }

                    {(data && gnomos.length==0) &&
                        <Text style={[styles.text, styles.title, {alignSelf:'center'}]}>
                            Gnome not found
                        </Text>
                    }
                </View>

                <ScrollView style={styles.scrollView}>
                    <View style={styles.items}>

                        <View style={styles.items}>
                            {gnomos}
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
