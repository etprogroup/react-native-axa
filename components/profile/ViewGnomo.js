/**
 * Detail Gnomo selected
 * */
import React from 'react';
import {
    Text,
    View,
    Picker,
    Image,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import styles from '../styles';

export default class ViewGnomo extends React.Component {

    /**
     * hidden Superior bar
     * */
    static navigationOptions = {
        title: 'Gnomos',
        header: null
    };

    constructor (props) {
        super(props);

        this.state={
            ...this.props,
            gnomo: this.props.navigation.getParam('gnomo', null),
        }

    }

    async componentDidMount(){

    }

    /**
     * return to select
     * */
    goToBack(){
        this.props.navigation.goBack();
    }


    render() {
        const {
            gnomo
        } = this.state;

        var name = 'Nameless';
        var friends = [];
        var works = [];

        return (
            <View style={styles.window}>
                <TouchableOpacity
                    onPress={()=>{this.goToBack()}}>
                    <View style={{}}>
                        <Text style={[styles.text,styles.title, {textAlign:'right'}]}>Back</Text>
                    </View>
                </TouchableOpacity>

                {(gnomo)&&
                <View style={styles.window}>
                    <View>
                        <Text style={[styles.text,styles.title]}>
                            Name: {gnomo.name}
                        </Text>

                        <Image
                            style={[styles.image,{alignSelf:'center'}]}
                            source={{uri:gnomo.thumbnail}}/>
                    </View>

                    <ScrollView style={styles.scrollView}>
                        <View style={styles.items}>
                            <Text style={[styles.text]}>
                                Age: {gnomo.age}
                            </Text>

                            <Text style={[styles.text]}>
                                Weight: {gnomo.weight}
                            </Text>

                            <Text style={[styles.text]}>
                                Height: {gnomo.height}
                            </Text>

                            <Text style={[styles.text]}>
                                Hair color: {gnomo.hair_color}
                            </Text>

                            <View style={[styles.items]}>
                                <Text style={[styles.text,styles.title]}>
                                    ({gnomo.professions.length}) Professions
                                </Text>
                                {gnomo.professions.map((work) => {
                                    return(
                                        <Text style={[styles.text]}>
                                            {work}
                                        </Text>
                                    )
                                })}
                            </View>


                            <View style={[styles.items]}>
                                <Text style={[styles.text,styles.title]}>
                                    ({gnomo.friends.length}) Firends
                                </Text>
                                {gnomo.friends.map((friend) => {
                                    return(
                                        <Text style={[styles.text]}>
                                            {friend}
                                        </Text>
                                    )
                                })}
                            </View>
                        </View>
                    </ScrollView>
                </View>
                }
            </View>
        );
    }
}
