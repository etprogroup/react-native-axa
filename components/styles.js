/**
 * centralize the styles of the app in an external file to the views
 * */
import {
    PixelRatio,
    StyleSheet
} from "react-native";

/**
 * Definition of colors
 * */
const color={
    primary: '#333',
    primaryLight: '#555',
    secondary: '#EEE',
    accent: '#000',
    disable: '#CCC',
}

export default styles = StyleSheet.create({
    window:{
        flex: 1,
        flexDirection:'column',
        justifyContent: 'space-between',
        backgroundColor:color.primary
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: color.secondary,
        overflow:'scroll'
    },
    scrollView: {
        flex: 1,
    },
    pickers:{
        flex: 1,
        flexDirection:'row',
        height: 50,
        margin:5,
    },
    itemPicker:{
        flex: 1,
    },
    picker:{
        flex: 1,
        height: 50,
    },
    text:{
        fontSize: 15,
        color: color.secondary,
        padding:10,
    },
    items:{
        margin:5,
    },
    item:{
        margin:5,
        backgroundColor: color.primaryLight,
    },
    title:{
        fontSize: 18,
        fontWeight: 'bold',
    },
    image:{
        borderRadius: 60,
        width: 120,
        height: 120,
        alignSelf:'center',
        backgroundColor: color.primary,
    },
    icon:{
        borderRadius: 30,
        width: 60,
        height: 60,
        alignSelf:'center',
        backgroundColor: color.primary,
    }
});