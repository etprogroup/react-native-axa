/**
 * It is created to manage the different views that the application has
 * */

import {createStackNavigator} from "react-navigation";
import ViewHome from "./home/ViewHome";
import ViewGnomo from "./profile/ViewGnomo";

export default AppNavigate = createStackNavigator(
    {
        Home: {
            screen: ViewHome,
        },
        ProfileGnomo: {
            screen: ViewGnomo,
        },
    },
    {
        headerMode: 'screen'
    },
    {
        initialRouteName: 'Home',
    }
);