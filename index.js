/**
 * Intro to App and redirect to first View with react-navigation
 * */

import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('axa', () => App);