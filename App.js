/**
 * Intro to App and redirect to first View with react-navigation
 * */

import React from 'react';
import AppNavigate from './components/AppNavigate';

export default class App extends React.Component {
    render() {
        return (
            <AppNavigate/>
        );
    }
}