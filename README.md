# AXA Test
Hybrid application developed with react-native by test requirements


## Library Used
```
react-navigation
```
It is created to manage the different views that the application has


## Views
```
ViewHome
```
View and Selection of Gnomos
This view has two pickers to filter by friends and professions to the gnomes

```
ViewGnomo
```
Detail Gnomo selected, with button for return to the list


### Components
```
styles
```
Centralize the styles of the app in an external file to the views

```
AppNavigate
```
It is created to manage the different views that the application has